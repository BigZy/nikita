﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BookLib.Models
{

    public class BookContext : DbContext
    {
        public BookContext()
            : base("name=BookContext")
        { }
        public DbSet<Book> Books { get; set; }
    }

    public class Book
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Author { get; set; }

        public string Genre { get; set; }

        public int Quantity { get; set; }

        public int Year { get; set; }

    }
}
