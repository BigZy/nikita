﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(LibBook.Startup))]
namespace LibBook
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
