﻿using System.Data.Entity;

namespace WebApplication.Models
{
    public class BookContext : DbContext
    {
        public DbSet<Book> Books { get; set; }
    }
}