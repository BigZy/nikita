﻿using Autofac;
using Autofac.Core;
using Autofac.Integration.Mvc;
using BookLib.Models;
using LibBook;
using LibBook.Models;
using System.Web.Mvc;

public class AutofacConfig
{
    public static void ConfigureContainer()
    {
        // получаем экземпляр контейнера
        var builder = new ContainerBuilder();

        // регистрируем контроллер в текущей сборке
        builder.RegisterControllers(typeof(MvcApplication).Assembly);

        builder.RegisterFilterProvider();
        builder.RegisterSource(new ViewRegistrationSource());
        
        // регистрируем споставление типов
        builder.RegisterType<BookRepository>().As<IRepository>();

        builder.RegisterModule(new DataModule("BookContext"));

        builder.RegisterType<BookRepository>()
        .As<IRepository>()
        .WithParameter("Context", new BookContext());
        // создаем новый контейнер с теми зависимостями, которые определены выше
        var container = builder.Build();

        // установка сопоставителя зависимостей
        DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
    }
}