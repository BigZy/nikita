﻿
using Autofac;
using Autofac.Core;
using BookLib.Models;

namespace LibBook
{
    public class DataModule : Module
    {
        private string _connStr;
        public DataModule(string connStr)
        {
            _connStr = connStr;
        }
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(c => new BookContext()).InstancePerRequest();
            base.Load(builder);
        }
   
    }

}